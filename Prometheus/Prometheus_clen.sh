


tar zxvf  node_exporter-0.18.1.linux-amd64.tar.gz
mkdir -pv  /data/Prometheus/node_exporter
cp  -avr    node_exporter-0.18.1.linux-amd64/* /data/Prometheus/node_exporter


cat  >>/etc/rc.local  << 'EOF'
cd  /data/Prometheus/node_exporter 
nohup ./node_exporter &
EOF


cat  >>/etc/rc.local  << 'EOF'
cd /data/Prometheus/prometheus
nohup ./prometheus --config.file=./prometheus.yml &
EOF