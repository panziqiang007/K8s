#!/bin/bash

IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."$3}')) ||IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."}'))
echo $IP
 yum install wget -y > /dev/null
####
GITHUB_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/prometheus/prometheus/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
[ ! $GITHUB_LATEST_VERSION ] && {
 while [ ture ] ;do
                    let u+=i
                    let i+=1
                    GITHUB_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/prometheus/prometheus/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')  
                    [ ! $GITHUB_LATEST_VERSION ] || break  1
                    echo  "重试次数${i}"
                    sleep 5
                done            
}
GITHUB_FILE="prometheus-${GITHUB_LATEST_VERSION//v/}.linux-amd64.tar.gz"
GITHUB_FILE_01=$(echo $GITHUB_FILE |sed 's/.tar.gz//g')
GITHUB_URL="https://github.com/prometheus/prometheus/releases/download/${GITHUB_LATEST_VERSION}/${GITHUB_FILE}"
##########
GITHUB_node_exporter_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/prometheus/node_exporter/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
[ ! $GITHUB_node_exporter_LATEST_VERSION ] && {
 while [ ture ] ;do
                    let u+=i
                    let i+=1
                    GITHUB_node_exporter_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/prometheus/node_exporter/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
                    [ ! $GITHUB_node_exporter_LATEST_VERSION ] || break  1
                    echo  "重试次数${i}"
                    sleep 5
                done            
}

GITHUB_node_exporter_FILE="node_exporter-${GITHUB_node_exporter_LATEST_VERSION//v/}.linux-amd64.tar.gz"
GITHUB_node_exporter_FILE_01=$(echo $GITHUB_node_exporter_FILE |sed 's/.tar.gz//g')
GITHUB_node_exporter_URL="https://github.com/prometheus/node_exporter/releases/download/${GITHUB_node_exporter_LATEST_VERSION}/${GITHUB_node_exporter_FILE}"
########################
#取grafana最新安装包
Granfa_FILE=$(curl -L -s -H 'Accept: application/json' https://mirrors.tuna.tsinghua.edu.cn/grafana/yum/rpm/  | awk -F '"'  '{print  $2}' |egrep -v "beta" | sed  '/^ *$/d'  |tail  -n 1)
Granfa_FILE_URL="https://mirrors.tuna.tsinghua.edu.cn/grafana/yum/rpm/${Granfa_FILE}"
echo  $GITHUB_LATEST_VERSION
echo  $GITHUB_FILE
echo  $GITHUB_URL
echo  $Granfa_FILE
echo  $Granfa_FILE_URL
##################
echo $GITHUB_node_exporter_LATEST_VERSION
echo $GITHUB_node_exporter_FILE
echo $GITHUB_node_exporter_FILE_01
echo $GITHUB_node_exporter_URL
####获取最新发行版
#下载最新版prometheus二进制包
if [  -f ./$GITHUB_FILE ]  
        then
             echo "当前目录已存在$GITHUB_FILE"
        else
            rm -fv $(ls prometheus*.tar.gz)
            echo "下载最新版$GITHUB_FILE"
                u=0 
                i=1
                while [ ture ] ;do
                    let u+=i
                    let i+=1
                    wget  $GITHUB_URL  && break  1
                    echo  "重试下载次数${i}"
                    sleep 1
                done         
fi

#下载最新版node_exporter二进制包
if [  -f ./$GITHUB_node_exporter_FILE ]  
        then
            
             echo "当前目录已存在$GITHUB_node_exporter_FILE"
        else
            rm -fv $(ls node_exporter*.tar.gz)
            echo "下载最新版$GITHUB_node_exporter_FILE"
                u=0 
                i=1
                while [ ture ] ;do
                    let u+=i
                    let i+=1
                    wget  $GITHUB_node_exporter_URL  && break  1
                    echo  "重试下载次数${i}"
                    sleep 1
                done         
fi


#下载最新版grafana二进制包
if [  -f ./$Granfa_FILE ]  
        then
             echo "当前目录已存在$Granfa_FILE"
        else
           rm -fv $(ls grafana*.rpm)
            echo "下载最新版$Granfa_FILE"
                u=0 
                i=1
                while [ ture ] ;do
                    let u+=i
                    let i+=1
                    wget  $Granfa_FILE_URL  && break  1
                    echo  "重试下载次数${i}"
                    sleep 1
                done         
fi


# GITHUB_FILE='prometheus-2.10.0.linux-amd64.tar.gz'
# GITHUB_node_exporter_FILE_01='node_exporter-0.18.1.linux-amd64.tar.gz'
# Granfa_FILE='grafana-6.2.5-1.x86_64.rpm'
#部署 Prometheus
mkdir  -pv  /data/Prometheus
tar zxvf  prometheus*.tar.gz
\mv  -v    $GITHUB_FILE_01  /data/Prometheus/prometheus   
# cat > /etc/systemd/system/prometheus.service  <<EOF

# cd /data/Prometheus/prometheus
# nohup ./prometheus --config.file=./prometheus.yml &
#安装饼图插件







#部署本机node_exporter
tar zxvf  node_exporter*.tar.gz
\mv  -v    $GITHUB_node_exporter_FILE_01  /data/Prometheus/node_exporter
cd  /data/Prometheus/node_exporter  && nohup ./node_exporter &
#

cat >/data/Prometheus/prometheus/prometheus.yml << EOF
global:
  scrape_interval:     15s
  evaluation_interval: 15s

alerting:
  alertmanagers:
  - static_configs:
    - targets:

rule_files:
scrape_configs:

  - job_name: 'node_1'
    static_configs:
    - targets: ['$IP:9100']
      labels:
        instance: '$IP'
EOF
cd /data/Prometheus/prometheus  && nohup ./prometheus --config.file=./prometheus.yml &


#granfa
yum install git  -y > /dev/null
yum install   ./grafana*x86_64.rpm  -y
git clone https://github.com/grafana/piechart-panel.git  /var/lib/grafana/plugins/piechart-panel
systemctl restart  grafana-server.service