#/bin/bash
#存储集群分区专用脚本
#检测可用的块设备
cd /root/K8s/glusterfs/
var_03=$(ansible  db -m shell -a  "lsblk | grep "disk""|grep -v CHANGED |awk '{print $1}'|sort -n |uniq)
for var in $var_03
    do
      echo "================$var,检测裸块设备,该设备下以下ip可用于glusterfs分布式存储使用,默认分出80%,剩余20%挂载到 /data目录作为数据目录使用============================"
      echo 
     ansible db -m shell -a  "lsblk  /dev/${var}|grep  -v NAME|wc   -l"|xargs  -n 1|egrep   -v  "CHANGED|rc|>|\|"|xargs  -n 2  | grep "1$"
     ansible db -m shell -a  "lsblk  /dev/${var}|grep  -v NAME|wc   -l"|xargs  -n 1|egrep   -v  "CHANGED|rc|>|\|"|xargs  -n 2  | grep "1$"|awk '{print  $1}'  > /root/K8s/ip_db.txt
     echo  $var >> /root/K8s/ip_db.txt
done
grep  "\."  /root/K8s/ip_db.txt||{
echo  "所选存储节点无可用块设备,k8s集群持久化部署终止Heketi+GlusterFS"
exit 4
}


disk_var01=$(tail -n 1 /root/K8s/ip_db.txt)
#40%作为k8s存储
cat > db.sh <<  'EHF'
_cc ()  {
/usr/bin/expect << EOF
spawn /usr/sbin/parted  /dev/ddd     mklabel gpt
expect {
        "Yes" {send "Yes\r";}       
}
expect eof 
EOF
}
_cc01  () {
/usr/bin/expect << EOF
spawn /usr/sbin/pvcreate  /dev/ddd1
expect {
        "Wipe" {send "y\r";}       
}     
expect eof 
EOF
}
###############
_cc
/usr/sbin/parted  /dev/ddd    mkpart primary 0%  90%
/usr/sbin/parted  /dev/ddd    mkpart primary 90%  100%
/usr/sbin/parted  /dev/ddd    p
while  [ true ]; do sleep 0.5; dmsetup remove   $(lsblk  -r /dev/ddd   |awk   '{print  $1}' |tac| grep '_');   lsblk  -r /dev/ddd     | grep '_' || break  1   ;done
sleep 3
mkfs.xfs  /dev/ddd1  -f
mkfs.xfs /dev/ddd2  -f
_cc01 
mkdir -pv /data
echo  "/dev/ddd2      /data      xfs       defaults            0        0  "  >>  /etc/fstab
mount  /dev/ddd2   /data
EHF

sleep 1
sed  "s/ddd/${disk_var01}/g"   db.sh  -i
#分区
# ansible db -m copy -a 'src=./db.sh dest=/opt/db.sh mode=755'
ansible db -m script -a "chdir=/tmp ./db.sh"
# ansible db  -m shell  -a  "test -s /opt/db.sh" || {
# ehco "文件为空,持久化存储失败"
# exit 2
# }
# ansible db -m shell -a  "sh /opt/db.sh"
ansible db -m shell -a   "df -h| grep /data"